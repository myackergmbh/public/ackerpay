#!/bin/bash

myTime=date "+%Y-%m-%d %H:%M:%S"
curl -v -H "Content-Type:application/json" --request POST "https://api.ackerpay.com/post/" --data '{
"api_key":"test",
"api_secret":"test",
"api_interface":"wingman",
"api_data":{
        "relay":"frontdoor",
        "function":"on"
},
"api_location":{
        "group":"GA01",
        "comp":"CA01",
        "loc":"A01"
},
"api_timezone":"Europe/Vienna",
"api_localtime":"'${myTime}'"}'

resultType=$(echo -n "${result}" | jq -r ".type")
if [ "${resultType}" == "success" ]; then
        echo "api call successfull!";
else
        echo "api call failed!";
        echo "$result"
fi
