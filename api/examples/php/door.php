<?php
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://api.ackerpay.com/post/');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$headers[] = 'Content-Type: application/json;charset=UTF-8';
$headers[] = 'Accept: application/json, text/plain, */*';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array(
    "api_key" => "test",
    "api_secret" => "test",
    "api_interface" => "wingman",
    "api_data" => array(
        "relay" => "frontdoor",
        "function" => "on"
    ),
    "api_location" => array(
        "group" => "GA01",
        "comp" => "GA01",
        "loc" => "A01"
    ),
    "api_timezone" => "Europe/Vienna",
    "api_localtime" => date("Y-m-d H:i:s")
)));

$result = curl_exec($ch);
if (curl_errno($ch)) {
    error_log(curl_error($ch));
    return false;
}
curl_close($ch);

$response = json_decode($result);
if($response["type"] == "success"){
    echo "api call successfull!";
}else{
    echo "api call failed!";
    var_dump($response);
}
