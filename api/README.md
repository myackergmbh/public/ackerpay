required info, provided by myacker:

| field | info | example |
| ------ | ------ | ------ |
| api_key | 32 characters | 098f6bcd4621d373cade4e832627b4f6 |
| api_secret | 128 characters | ee26b0dd4af7e749aa1a8ee3c10ae9923f... |
| group | 4 characters | GA00 |
| comp | 4 characters | CA00 |
| loc | 3 characters | A00 |

to try out the api, use `test` for the api_key and api_secret

json returned:
```
{
  "type": "success/error/debug",
  "data": {
    "user": [
      "user ID",
      "user login",
      "user name"
    ],
    "result": {
      "interface": "interface name",
      "data": {
        "data for interface"
      },
      "timezone": "Europe\\/Berlin",
      "localtime": "local time in Y-m-d H:i:s"
    }
  },
  "debug": "debug info"
}
```

tested on debian10, archlinux (2021-12) with >= php7.0

required modules: php-curl

optional: [jq](https://stedolan.github.io/jq/tutorial/) for bash to parse json
